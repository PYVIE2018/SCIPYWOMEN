# Scientific Python for Women

A one day workshop for women focusing on Scientific Python. 

## Environment

To replicate the environment `conda env create -f env.yml` or 
take a look at the file.

## Run All

All notebooks can be executed using `jupyter nbconvert --to notebook --execute notebook.ipynb`
